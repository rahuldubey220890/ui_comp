(function() {
    'use strict';

    angular
        .module('agVantageApp')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log) {

        $log.debug('runBlock end');
    }

})();