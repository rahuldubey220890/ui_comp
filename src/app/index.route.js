(function() {
    'use strict';

    angular
        .module('agVantageApp')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            
            .state('login', {
                url: '/login',
                templateUrl: 'app/components/login/login.html',
                controller: 'LoginController',
                controllerAs: 'login'
            }).state('signup', {
                url: '/signup',
                templateUrl: 'app/components/signup/signup.html',
                controller: 'SignUpController',
                controllerAs: 'signup'
            }).state('signup.step1', {
                url: '/step1',
                templateUrl: 'app/components/signup/steps/step1.html'
            })

            // url will be /form/interests
            .state('signup.step2', {
                url: '/step2',
                templateUrl: 'app/components/signup/steps/step2.html'
            })

            // url will be /form/payment
            .state('signup.step3', {
                url: '/step3',
                templateUrl: 'app/components/signup/steps/step3.html'
            })
            .state('signup.step4', {
                url: '/step3',
                templateUrl: 'app/components/signup/steps/step4.html'
            })
            .state('signup.step5', {
                url: '/step3',
                templateUrl: 'app/components/signup/steps/step5.html'
            });


        $urlRouterProvider.otherwise('/login');
    }

})();