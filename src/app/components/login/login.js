(function() {
    'use strict';

    angular
        .module('agVantageApp')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($scope) {
    	$scope.reasons=[
            {img:"fa fa-exchange",why:"Best price discovery"},
    		{img:"fa fa-exchange",why:"Marketing Management tool on the market"},
    	    {img:"fa fa-exchange",why:"24/7 online account access"},
            {img:"fa fa-exchange",why:"Fast and easy to use"},
            {img:"fa fa-exchange",why:"Highly secure"}];
    }
})();
