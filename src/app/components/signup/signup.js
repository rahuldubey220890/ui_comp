(function() {
    'use strict';

    angular
        .module('agVantageApp')
        .controller('SignUpController', SignUpController);

    /** @ngInject */
    function SignUpController($scope) {
    	$scope.formData = {};
    }
})();