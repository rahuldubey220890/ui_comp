(function() {
    'use strict';

    angular
        .module('agVantageApp', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap']);

})();